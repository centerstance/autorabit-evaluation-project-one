@RestResource(urlMapping='/TestPost/*')
global with sharing class testRestPost {
	public class RequestValues{
		public String AccountName {get; set;}
		public String AccountCode {get; set;}
		public String FirstName {get; set;}
		public String LastName {get; set;}
	}
	
	@HttpPost
	global static String createAccountContact(){
		String ReturnValue = null;
		RestRequest request = RestContext.request;
		String values = request.requestBody.toString();
		List<RequestValues> deserialize = (List<RequestValues>)JSON.deserialize(values, List<RequestValues>.class);
		List<Account> upsertAccount = new List<Account>();
		for(RequestValues req : deserialize){
			Account a = new Account();
			a.Name = req.AccountName;
			a.Account_Code__c = req.AccountCode;
			upsertAccount.add(a);
		}
		
		if(upsertAccount.isEmpty() == false){
			upsert upsertAccount;
		}
		
		return 'true';
	}
}