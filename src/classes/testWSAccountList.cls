global class testWSAccountList {
	webService static List<Account> getActiveAccounts(){
		List<Account> accts = [Select Id, Name From Account Where AccountNumber != null And Active__c = 'Yes'];
		
		return accts;
	}
	
	webService static String getAccountNumber(Id AccountId){
		List<Account> accts = [Select AccountNumber From Account Where Id = :AccountId];
		String AccountNumber = null;
		
		if(accts.isEmpty() == false){
			AccountNumber = accts[0].AccountNumber;
		}
		
		return AccountNumber;
	}
}