global class testWSCreateAccount2 {
	global class Values {
    	public string AccountName { get; set; }
    	public string AccountCode  { get; set; }
    	public string FirstName  { get; set; }
    	public string LastName  { get; set; }
    }
    
	webservice static Boolean createAccountContact(List<Values>Accounts){
		for(Values v : Accounts){
			Account a = new Account(Name = v.AccountName, Account_Code__c=v.AccountCode);
			Upsert a;
			
			Contact c = new Contact(AccountId=a.Id, LastName=v.LastName, FirstName=v.FirstName);
			insert c;
		}
		
		return true;		
	}
}