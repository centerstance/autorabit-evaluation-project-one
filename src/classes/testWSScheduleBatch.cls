global with sharing class testWSScheduleBatch {
	webservice static Boolean executeBatchApex(){
		testBatch oBatch = new testBatch();
		Id idBatchProcess = database.executeBatch(oBatch);
		
		return true;		
	}
}