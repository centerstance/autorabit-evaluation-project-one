global class testWSCreateAccount {
	webservice static Boolean createAccountContact(String AccountName, String Code, String FirstName, String LastName){
		Account a = new Account(Name = AccountName, Account_Code__c=Code);
		Upsert a;
		
		Contact c = new Contact(AccountId=a.Id, LastName=LastName, FirstName=FirstName);
		insert c;
		
		return true;		
	}
}