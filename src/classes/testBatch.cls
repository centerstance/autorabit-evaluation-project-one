global class testBatch implements Database.Batchable<sObject>{
	global Database.Querylocator start(Database.BatchableContext BC){
		return Database.getQueryLocator([Select Id, Name, SicDesc From Account Limit 100]);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope){
		List<Account> accts = new List<Account>();
		for(sObject s : scope){
			Account a =(Account)s;
			a.SicDesc = system.now().format();
			accts.add(a);
		}
		update accts;
	}
	
	global void finish(Database.BatchableContext info){
		
	}
}