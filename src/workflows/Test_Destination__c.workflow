<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Lookup_Value</fullName>
        <field>Lookup_Value__c</field>
        <formula>CASE(Pick_List_Selection__c,&quot;One&quot;,&quot;1&quot;,&quot;Two&quot;,&quot;2&quot;,&quot;Three&quot;,&quot;3&quot;,&quot;Four&quot;,&quot;4&quot;,&quot;*UNK*&quot;)</formula>
        <name>Update Lookup Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Lookup Value</fullName>
        <actions>
            <name>Update_Lookup_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Pick_List_Selection__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
