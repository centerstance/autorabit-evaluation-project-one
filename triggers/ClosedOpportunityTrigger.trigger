trigger ClosedOpportunityTrigger on Opportunity (after insert, after update) {
    List<Task> taskList = new List<Task>();
    
    for(Opportunity o : trigger.new){
        if(o.StageName == 'Closed Won'){
            taskList.add(new Task(Subject = 'Follow Up Test Task', WhatId = o.Id));
        }
    }
    
    if(taskList.isEmpty() == false){
    	insert taskList;
    }
}